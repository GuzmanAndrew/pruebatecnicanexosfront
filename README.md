# Script Base de datos
## Crear Tabla Agregar Producto
```sh
create table agregar(
	id serial not null,
	nombre varchar(60) not null,
	cantidad int not null,
	dateRegister varchar(60) not null,
	usuarioRegister varchar(60) not null,
	constraint pk_agregar PRIMARY KEY(id)
);
```
## Crear Tabla Editar Producto
```sh
create table editar(
	id serial not null,
	nombre varchar(60) not null,
	total varchar(60) not null,
	dateRegister varchar(60) not null,
	usuarioRegister varchar(60) not null,
	dateEdit varchar(60) not null,
	usuarioEdit varchar(60) not null,
	constraint pk_editar PRIMARY KEY(id)
);
```
