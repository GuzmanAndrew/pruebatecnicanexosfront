import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';
import { ProductoUpdate } from 'src/app/Model/ProductoUpdate';
import { Producto } from 'src/app/Model/Producto';

@Component({
  selector: 'app-person-update',
  templateUrl: './person-update.component.html',
  styleUrls: ['./person-update.component.css']
})
export class PersonUpdateComponent implements OnInit {

  persona: Producto = {
    id: 0,
    nombre: '',
    cantidad: 0,
    dateregister: '',
    usuarioregister: '',
  };

  productoUpdate: ProductoUpdate = this.persona;

  constructor(private service: ServiceService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.service.detail(params.id).subscribe(
        res => {
          this.persona = res;
        },
        err => console.error(err)
      );
    }
  }



  updatePerson() {
    this.service.updatePersona(this.persona.id, this.productoUpdate).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/person']);
      },
      err => console.error(err)
    );
  }
}
