import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Producto } from 'src/app/Model/Producto';

import { ServiceService } from '../../Service/service.service';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit {

  persona: Producto = {
    id: 0,
    nombre: '',
    cantidad: 0,
    dateregister: '',
    usuarioregister: ''
  };

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {}

  savePerson() {
    this.service.save(this.persona).subscribe(
      res => {
        this.router.navigate(['/person']);
      },
      err => console.error(err)
    );
  }
}
