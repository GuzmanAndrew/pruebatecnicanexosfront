import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../Service/service.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  productos: any = [];

  constructor(private service: ServiceService, private router: Router,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.getPersons();
  }

  getPersons() {
    this.service.lista().subscribe(
      res => {
        console.log(res);
        this.productos = res;
      },
      err => console.error(err)
    );
  }

  deletePerson(id: number) {
    this.service.delete(id).subscribe(
      res => {
        console.log(res);
        this.getPersons();
      },
      err => console.error(err)
    );
  }

}