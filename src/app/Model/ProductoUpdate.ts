export class ProductoUpdate {
   id?: number;
   nombre?: string;
   cantidad?: number;
   dateregister?: string;
   usuarioregister?: string;
   dateedit?: string;
   usuarioedit?: string;
}
