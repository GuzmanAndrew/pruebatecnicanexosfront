import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../Model/Producto';
import { ProductoUpdate } from '../Model/ProductoUpdate';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  url = 'http://localhost:8081/api';

  constructor(private http: HttpClient) { }

  lista(){
    return this.http.get(`${this.url}/listar/productos`);
  }

  detail(id: number){
    return this.http.get(`${this.url}/producto/${id}`);
  }

  detailUpdate(id: number){
    return this.http.get(`${this.url}/productoUpdate/${id}`);
  }

  save(agregar: Producto){
    return this.http.post(`${this.url}/guardar/producto`, agregar);
  }

  delete(id: number){
    return this.http.delete(`${this.url}/producto/${id}`);
  }

  updatePersona(id: number, updateProducto: ProductoUpdate) {
    return this.http.put(`${this.url}/actualizar/${id}`, updateProducto);
  }
}
